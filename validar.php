<?php
	echo '
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Actividad 2</title>
		<meta charset="utf-8">  
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="style.css">
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>
	<body>
		<h1 align="center" class="fadeIn first">Aguilar Bernal Luis Raúl - 12/DIC/20</h1>
	';
	session_start();
	include("clsConexion.php");
	include("clsValidacion.php");
	
	//Se abre la conexion con la base de datos
	$objconexion = new clsConexion();
	$conexion=$objconexion->Open();
	
	//Si se recibieron los datos correctamente continua la ejecucion
	if(isset( $_POST["enviar"]) && $_SERVER["REQUEST_METHOD"] == "POST" ){
	
		$vNombre = $_POST["nombre"];
		$vApellido = $_POST["apellido"];
		$vCelular = $_POST["celular"];
		$vCorreo = $_POST["email"];
		$vMensaje = $_POST["mensaje"];		
		
		//Se validan los datos recibidos
		$objMensajeserror=array();
		$objValidacion = new clsValidacion();		
		$objMensajeserror = $objValidacion->mCampoVacio($vNombre,'nombre');
		$objMensajeserror = $objValidacion->mCampoVacio($vApellido,'apellido');
		$objMensajeserror = $objValidacion->mCampoVacio($vCelular,'celular');
		$objMensajeserror = $objValidacion->mCampoVacio($vCorreo,'email');
		$objMensajeserror = $objValidacion->mCampoVacio($vMensaje,'mensaje');
		
		if(isset($objValidacion->objMensajeserror)){
			echo "Favor de corregir lo siguiente e intentarlo de nuevo: <br>";
			foreach($objValidacion->$objMensajeserror as $error){
				$Contador++;
				echo "<li>Error No. ".$Contador."</li>";
			}
			echo "<a href='index.php'>Regresar</a>";
		}else{
			//Consulta para ingresar la información capturada en la base de datos
			$consulta = $conexion->prepare('insert into mensajes values (null,"'.$vNombre.'","'.$vApellido.'","'.$vCelular.'","'.$vCorreo.'","'.$vMensaje.'")');		
			//Se se ejecuto la consulta correctamente
			if($consulta->execute()){
				$_SESSION['id'] = $conexion->lastInsertId();
				$_SESSION['nombre'] = $vNombre;
				$_SESSION['apellido'] = $vApellido;
				//redirige a otra página donde se muestra el código QR y una tabla con mensajes anteriores
				header("Location: tabla.php");
				exit();
			}else{//si no se realizó correctamente muestra un mensaje error
				echo "Error al guardar en la base de datos";
			}
			$objconexion->Close();
		}
	}
?>