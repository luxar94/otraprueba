<!DOCTYPE html>
<html lang="en">
<head>
	<title>Actividad 2</title>
	<meta charset="utf-8">  
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style.css">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<h1 align="center" class="fadeIn first">Aguilar Bernal Luis Raúl - 12/DIC/20</h1>
<div class="wrapper fadeInDown">
  <div id="formContent">
	<h1>Iniciar Sesion</h1>	
    <form id="form1" name="form1" method="post" action="mensaje.php" class="was-validated">
		<label for="usuario" class="fadeIn first" >Usuario</label> 
		<input type="text" id="login" class="fadeIn second" name="usuario" id="usuario" placeholder="Ingrese su usuario" required>		
		<label for="password" class="fadeIn third" >Contraseña</label>
		<input type="password" id="password" class="fadeIn fourth" name="password" id="password" placeholder="Ingrese su contraseña" required>
		<label for="captcha_code" class="fadeIn fifth" >Captcha</label>
		<img src="GenerarCaptcha.php" class="fadeIn fifth">		
		<input type="text" class="fadeIn sixth" name="captcha_code" id="captcha_code"  placeholder="Ingrese los numeros" required>		
		<input type="submit" name="enviar" class="fadeIn seventh" class="btn btn-primary" value="Siguiente">
    </form>
  </div>
</div>
</body>
</html>