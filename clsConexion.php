<?php	
	class clsConexion{
		/**
		 * [$server variable con el nombre del servidor]
		 * @var string
		 */
		private $server="mysql:host=localhost";
		/**
		 * [$user variable con el nombre de usuario de la bd]
		 * @var string
		 */
		private $user="root";
		/**
		 * [$password variable con la contraseña de la bd]
		 * @var string
		 */
		private $password="";
		/**
		 * [$con variable de conexión a la bd]
		 * @var [object]
		 */
		protected $con;
		/**
		 * [__construct Constructor de la clase]
		 */
		function __construct(){
			$server="mysql:host=localhost";
			$user="root";
			$password="";
		}		
		/**
		 * [Open Abre la conexión a la base de datos almacen]
		 * retorna la variable $con con la conexión
		 */
		public function Open(){
			$server="mysql:host=localhost";
			$user="root";
			$password="";
			try{				
				$con = new PDO($server.";dbname=actividad2",$user,$password);				
			} catch (PDOException $e){
				//mensaje de error:
				echo "Error: ". $e->getMessage();
			}
			return $con;
		}
		
		/**
		 * [Close cierra la conexión a la base de datos]
		 */
		public function Close(){
			$con=NULL;
		}		
		
		}
?>