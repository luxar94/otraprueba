<?php
	echo '
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Actividad 2</title>
		<meta charset="utf-8">  
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="style.css">
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head><body><h1 align="center" class="fadeIn first">Aguilar Bernal Luis Raúl - 12/DIC/20</h1>
	';
	session_start();
	include("clsConexion.php");
	include("clsValidacion.php");
	
	//Variable Booleana usada para señalar si se inicio sesion o no de manera correcta.
	$bSesion = false;
	
	//Se abre la conexion con la base de datos
	$objconexion = new clsConexion();
	$conexion=$objconexion->Open();
	
	//Si se recibieron los datos correctamente continua la ejecucion
	if(isset( $_POST["enviar"]) && $_SERVER["REQUEST_METHOD"] == "POST" ){		
		
		$vUsuario = $_POST["usuario"];
		$vPassword = $_POST["password"];
		$vCaptcha = $_POST["captcha_code"];
		
		//Se validan los datos recibidos
		$objMensajeserror=array();
		$objValidacion = new clsValidacion();				
		$objMensajeserror = $objValidacion->mCampoVacio($vUsuario,'usuario');
		$objMensajeserror = $objValidacion->mCampoVacio($vPassword,'password');
		$objMensajeserror = $objValidacion->mCampoVacio($vCaptcha,'captcha_code');
		
		if(isset($objValidacion->objMensajeserror)){
			echo "Favor de corregir lo siguiente e intentarlo de nuevo: <br>";
			foreach($objValidacion->$objMensajeserror as $error){
				$Contador++;
				echo "<li>Error No. ".$Contador."</li>";
			}
			echo "<a href='index.php'>Regresar</a>";
		}else{
			//Consulta para verificar si los datos ingresados coinciden con los de algun usuario
			$consulta = $conexion->prepare('select usuario, password, nombre, apellido, correo from usuarios');		
			//Se se ejecuto la consulta correctamente
			if($consulta->execute()){				
				$resultado = $consulta->fetchAll();
				foreach($resultado as $fila){
					//Si el captcha es correcto, asi como el usuario y contraseña brindados muestra el formulario
					if($fila['usuario'] == $vUsuario && $fila['password'] == $vPassword && $_POST["captcha_code"] == $_SESSION["captcha_code"] ){					
						$bSesion = true;
						echo '
							<div class="wrapper fadeInDown">
							<div id="formContent">
								<h1>Mensaje</h1>							
								<form id="form2" name="form2" method="post" action="validar.php" class="was-validated">
									<label for="nombre" class="fadeIn first" >Nombre</label>
									<input type="text" class="fadeIn second" name="nombre" id="nombre" value="'.$fila['nombre'].'" required>
									<label for="apellido" class="fadeIn first" >Apellido</label>
									<input type="text" class="fadeIn fourth" name="apellido" id="apellido" value="'.$fila['apellido'].'" required>
									<br><label for="celular" class="fadeIn second" >Celular</label><br>
									<input type="tel" pattern="[0-9]{10}" class="fadeIn fourth" name="celular" id="celular" placeholder="##########" required>
									<label for="correo" class="fadeIn first" >Correo Electronico</label>
									<input type="email" class="fadeIn second" name="email" id="email" value="'.$fila['correo'].'" required>
									<label for="mensaje" class="fadeIn first" >Mensaje</label>
									<input type="text-area" class="fadeIn second" name="mensaje" id="mensaje" placeholder="Escriba el mensaje deseado" required>
									<input type="submit" name="enviar" class="fadeIn third" class="btn btn-primary" value="Siguiente">
								</form>
							</div>
							</div>
						';
					}				
				}
				//Si no es correcto muestra un mensaje y un enlace para regresar
				if(!$bSesion){
					echo'
					<div class="wrapper fadeInDown">
						<h3>Los datos ingresados no son correctos intentelo de nuevo</h3>
						<div id="regresar">
							<a class="underlineHover" href="index.php">Regresar</a>
						</div>
					</div>
					</body>
					';
				}	
				//exit();
			}else{//si no se realizó correctamente muestra un mensaje error
				echo "Error al consultar con la base de datos";
			}
					
			$objconexion->Close();
			
		}
	}
?>