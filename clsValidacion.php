<?php
/**
 * Clase clsValidacion
 * Materia: Desarrollo Web 2
 * @author Aguilar Bernal Luis Raúl <[<lraul.ab@gmail.com>]>
 */
class clsValidacion{
    
    public $objMensajesError = array();
	function __construct(){ }
	
	/**
     * Este método valida que el dato capturado por el usuario no tenga caracteres especiales y elimina los espacios en blanco que lo antecedes y suceden.
     * @param  [String] $vVariable es la variable que viene del formulario.
     * @return [String] Devuelve la $vVariable sin espacios o caracteres especiales.
     */
	function mLimpiarVariable($vVariable){
		$vVariable = trim($vVariable); // Elimina espacios antes y despues de los datos
		$vVariable = stripslashes($vVariable); // Elimina backslashes \
		$vVariable = htmlspecialchars($vVariable); // Traduce caracteres en entidades HTML
		return $vVariable;		
	}
	
	/**
     * Este método valida que un campo del formulario no venga vacío.
     * @param  [String] $vVariable    es la variable que viene del formulario.
     * @param  [String] $vNombreCampo  es el nombre del campo del formulario.
     * @return [Array] $objMensajesError es el arreglo que contiene el mensaje de error.
     */
	function mCampoVacio($vVariable, $vNombreCampo){
		$objMensajesError=NULL;
		if(empty($vVariable))
			$this->$objMensajesError[]="El campo ".$vNombreCampo." es necesario.";
		return $objMensajesError;
	}
	
	/**
     * [Este método valida que el correo no venga vacío y que el formato sea el correcto]
     * @param  [String] $vEmail es el email capturado por el usuario.
     * @return [array] $objMensajeserror es el arreglo que contiene el mensaje de error.
     */
	function mVerificarEmail($vEmail){
		$objMensajesError=NULL;
		// El email es obligatorio y ha de tener formato adecuado
		if(!filter_var($vEmail, FILTER_VALIDATE_EMAIL) || empty($vEmail))
			$this->$objMensajesError[] = "No se ha indicado email o el formato no es correcto.";
		return $objMensajesError;
	}	
}
?>