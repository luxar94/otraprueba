<?php
	echo '
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Actividad 2</title>
		<meta charset="utf-8">  
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
	</head>
	<body>
		<h1 align="center">Aguilar Bernal Luis Raúl - 12/DIC/20</h1>
	';
	session_start();
	include('./phpqrcode/qrlib.php');
	include("clsConexion.php");
	$objconexion = new clsConexion();
	$conexion=$objconexion->Open();
	
	if(isset($_SESSION['id'])){
		$vID = $_SESSION['id'];
		$vNombre = $_SESSION['nombre'];
		$vApellido = $_SESSION['apellido'];
		$contenido="ID: ".$_SESSION['id']." Nombre: ".$_SESSION['nombre']." Apellido: ".$_SESSION['apellido'];		
		QRcode::png($contenido, "./QRCODE.png");
		 echo '
			<p>Guardado con exito, el siguiente código QR contiene la información capturada</p>
			<img src="./QRCODE.png" />
			<h2>Mensajes anteriores:</h2>
			';
		echo '
			<table class="table">
				<thead>
					<tr>						
						<th scope="col">Nombre</th>
						<th scope="col">Apellido</th>
						<th scope="col">Celular</th>
						<th scope="col">Correo</th>						
					</tr>
				</thead>
				<tbody>
				';
		
		$consulta = $conexion->prepare('select nombre, apellido, celular, correo, mensaje from mensajes');
		$consulta->execute();
		$resultado = $consulta->fetchAll();
		foreach($resultado as $fila){
			echo '
				<tr>
					<td>'.$fila['nombre'].'</td>
					<td>'.$fila['apellido'].'</td>
					<td>'.$fila['celular'].'</td>
					<td>'.$fila['correo'].'</td>					
				</tr>
				<tr>
					<th scope="row">'.$fila['mensaje'].'</th>
				</tr>
			';
		}		
		echo '
				</tbody>
			</table>
		';		
	}else{
		echo "Error al obtener el ID intentelo otra vez.";
		echo "<a href='index.php'>Regresar</a>";
	}

?>