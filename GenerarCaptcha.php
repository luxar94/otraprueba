<?php
	$ancho = 60;
	$alto = 30;
	$fondo_imagen = 15;
	$CodigoCaptchaAleatorio=rand(1000,9999);	
	session_start();
	$_SESSION['captcha_code']=$CodigoCaptchaAleatorio;
	$im = imagecreatetruecolor($ancho, $alto);
	$bg = imagecolorallocate($im, 230, rand(1,255), 0);
	$fg = imagecolorallocate($im, 255, 255, 255);
	$ns = imagecolorallocate($im, 200, 200, 200);
	imagefill($im, 0, 0, $bg);
	imagestring($im, 5, 10, 8, $CodigoCaptchaAleatorio, $fg);
	for($i = 0; $i < $fondo_imagen; $i++){
		for($j = 0; $j < $fondo_imagen; $j++){
			imagesetpixel($im, rand(0,$ancho),rand(0,$alto),$ns);
		}
	}
	header("Cache-Control: no-cache, must-revalidate");
	header("Content-type: image/png");
	imagepng($im);
	imagedestroy($im);
?>